-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2022 at 08:42 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hris_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_gapok`
--

CREATE TABLE `ref_gapok` (
  `ID_GAPOK` int(11) NOT NULL,
  `KODE` varchar(25) NOT NULL,
  `NAMA` varchar(100) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `CREATE_BY` varchar(100) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_BY` varchar(100) NOT NULL,
  `UPDATE_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_gapok`
--

INSERT INTO `ref_gapok` (`ID_GAPOK`, `KODE`, `NAMA`, `ACTIVE`, `CREATE_BY`, `CREATE_DATE`, `UPDATE_BY`, `UPDATE_DATE`) VALUES
(1, '890w', 'Test', 1, 'DEVELOPER', '2021-01-01', 'DEVELOPER', '2021-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `ref_permohonan_cuti`
--

CREATE TABLE `ref_permohonan_cuti` (
  `ID_PERMOHONAN_CUTI` int(11) NOT NULL,
  `KODE` varchar(30) NOT NULL,
  `ID_PEGAWAI` varchar(30) NOT NULL,
  `TANGGAL_PERMOHONAN` date NOT NULL,
  `LAMA_CUTI` varchar(20) NOT NULL,
  `SEJAK_TANGGAL` date NOT NULL,
  `HINGGA_TANGGAL` date NOT NULL,
  `KETERANGAN` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_permohonan_cuti`
--

INSERT INTO `ref_permohonan_cuti` (`ID_PERMOHONAN_CUTI`, `KODE`, `ID_PEGAWAI`, `TANGGAL_PERMOHONAN`, `LAMA_CUTI`, `SEJAK_TANGGAL`, `HINGGA_TANGGAL`, `KETERANGAN`) VALUES
(1, 'test1', 'test1', '2022-06-15', '3', '2022-06-22', '2022-06-24', 'Healing');

-- --------------------------------------------------------

--
-- Table structure for table `ref_permohonan_pinjaman`
--

CREATE TABLE `ref_permohonan_pinjaman` (
  `ID_PERMOHONAN_PINJAMAN` int(11) NOT NULL,
  `KODE_PINJAMAN` varchar(30) NOT NULL,
  `ID_PEGAWAI` varchar(30) NOT NULL,
  `TANGGAL_PERMOHONAN` date NOT NULL,
  `JUMLAH_PINJAMAN` int(20) NOT NULL,
  `LIMIT_PINJAMAN` int(20) NOT NULL,
  `KETERANGAN` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_permohonan_pinjaman`
--

INSERT INTO `ref_permohonan_pinjaman` (`ID_PERMOHONAN_PINJAMAN`, `KODE_PINJAMAN`, `ID_PEGAWAI`, `TANGGAL_PERMOHONAN`, `JUMLAH_PINJAMAN`, `LIMIT_PINJAMAN`, `KETERANGAN`) VALUES
(1, 'test3', 'test3', '2022-06-15', 3000000, 5000000, 'Biaya refreshing');

-- --------------------------------------------------------

--
-- Table structure for table `ref_permohonan_resign`
--

CREATE TABLE `ref_permohonan_resign` (
  `ID_PERMOHONAN_RESIGN` int(11) NOT NULL,
  `KODE_PERMOHONAN` varchar(30) NOT NULL,
  `ID_PEGAWAI` varchar(20) NOT NULL,
  `TANGGAL_PERMOHONAN` date NOT NULL,
  `TANGGAL_PENGAJUAN` date NOT NULL,
  `KETERANGAN` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_permohonan_resign`
--

INSERT INTO `ref_permohonan_resign` (`ID_PERMOHONAN_RESIGN`, `KODE_PERMOHONAN`, `ID_PEGAWAI`, `TANGGAL_PERMOHONAN`, `TANGGAL_PENGAJUAN`, `KETERANGAN`) VALUES
(1, 'test2', 'test2', '2022-06-16', '2022-07-01', 'Ingin membuka lembaran baru');

-- --------------------------------------------------------

--
-- Table structure for table `ref_posisi_pegawai`
--

CREATE TABLE `ref_posisi_pegawai` (
  `ID_POSISI_PEGAWAI` int(11) NOT NULL,
  `KODE` varchar(25) NOT NULL,
  `NAMA` varchar(100) NOT NULL,
  `ID_GAPOK` int(11) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `CREATE_BY` varchar(100) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_BY` varchar(100) NOT NULL,
  `UPDATE_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ref_status_pegawai`
--

CREATE TABLE `ref_status_pegawai` (
  `ID_STATUS_PEGAWAI` int(11) NOT NULL,
  `KODE` varchar(25) NOT NULL,
  `NAMA` varchar(50) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `CREATE_BY` varchar(100) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `UPDATE_BY` varchar(100) NOT NULL,
  `UPDATE_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ref_gapok`
--
ALTER TABLE `ref_gapok`
  ADD PRIMARY KEY (`ID_GAPOK`);

--
-- Indexes for table `ref_permohonan_cuti`
--
ALTER TABLE `ref_permohonan_cuti`
  ADD PRIMARY KEY (`ID_PERMOHONAN_CUTI`);

--
-- Indexes for table `ref_permohonan_pinjaman`
--
ALTER TABLE `ref_permohonan_pinjaman`
  ADD PRIMARY KEY (`ID_PERMOHONAN_PINJAMAN`);

--
-- Indexes for table `ref_permohonan_resign`
--
ALTER TABLE `ref_permohonan_resign`
  ADD PRIMARY KEY (`ID_PERMOHONAN_RESIGN`);

--
-- Indexes for table `ref_posisi_pegawai`
--
ALTER TABLE `ref_posisi_pegawai`
  ADD PRIMARY KEY (`ID_POSISI_PEGAWAI`);

--
-- Indexes for table `ref_status_pegawai`
--
ALTER TABLE `ref_status_pegawai`
  ADD PRIMARY KEY (`ID_STATUS_PEGAWAI`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ref_gapok`
--
ALTER TABLE `ref_gapok`
  MODIFY `ID_GAPOK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_permohonan_cuti`
--
ALTER TABLE `ref_permohonan_cuti`
  MODIFY `ID_PERMOHONAN_CUTI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_permohonan_pinjaman`
--
ALTER TABLE `ref_permohonan_pinjaman`
  MODIFY `ID_PERMOHONAN_PINJAMAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_permohonan_resign`
--
ALTER TABLE `ref_permohonan_resign`
  MODIFY `ID_PERMOHONAN_RESIGN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_posisi_pegawai`
--
ALTER TABLE `ref_posisi_pegawai`
  MODIFY `ID_POSISI_PEGAWAI` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ref_status_pegawai`
--
ALTER TABLE `ref_status_pegawai`
  MODIFY `ID_STATUS_PEGAWAI` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
