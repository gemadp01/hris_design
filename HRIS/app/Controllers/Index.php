<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index(){
        echo view('Partial/Header');
        echo view('Index');
        echo view('Partial/Footer');
    }
}