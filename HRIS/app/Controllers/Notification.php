<?php

namespace App\Controllers;

class Notification extends BaseController
{
    public function index(){
        echo view('Partial/Header');
        echo view('Notification/Index');
        echo view('Partial/Footer');
    }

}
