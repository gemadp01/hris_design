<?php

namespace App\Controllers;

use App\Models\RefCutiModel;
use App\Models\RefResignModel;
use App\Models\RefPinjamanModel;
class Request extends BaseController
{
    public function index(){
        echo view('Partial/Header');
        echo view('Request/Index');
        echo view('Partial/Footer');
    }

    public function TestCuti(){
        $refCutiModel = new RefCutiModel();
        $data = $refCutiModel->findAll();
        dd($data);
    }

    public function TestResign(){
        $refResignModel = new RefResignModel();
        $data = $refResignModel->findAll();
        dd($data);
    }

    public function TestPinjaman(){
        $refPinjamanModel = new RefPinjamanModel();
        $data = $refPinjamanModel->findAll();
        dd($data);
    }

}
