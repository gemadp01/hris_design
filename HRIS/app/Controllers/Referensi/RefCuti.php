<?php

namespace App\Controllers\Referensi;
use App\Controllers\BaseController;

use App\Models\RefCutiModel;

class RefCuti extends BaseController
{
    protected $refCutiModel;
    public function __construct(){
        $this->refCutiModel = new RefCutiModel();
    }

    public function GetData($id = false){
        $data = $this->refCutiModel->getData($id);
        dd($data);
    }

}