<?php

namespace App\Controllers;

class Personal extends BaseController
{
    public function index(){
        echo view('Partial/Header');
        echo view('Personal/Index');
        echo view('Partial/Footer');
    }

}
