﻿    <div class="">
        <main role="main">
            <div>
                <div class="head_attenddance-content">
                    <div class="container" style="margin-bottom: 30px;">
                        <p class="text-center mt-3 pt-3 mb-0 pb-0">Live Attenddance</p>
                        <h3 id="jam" class="text_attenddance-time_now">
                        </h3>
                        <p id="tgl" class="text_attenddance-datetime_now"></p>
                    </div>
                    <div class="panel_attenddance md">
                        <div class="row p-0 m-0">
                            <div class="container">
                                <input type="checkbox" class="toggle m-0 p-0" id="rounded">
                                <label for="rounded" data-checked="WFO" class="rounded-check m-0 p-0" data-unchecked="WFH"></label>
                            </div>
                        </div>
                        <p class="text_attenddance-title2 md p-0">Work From Office</p>
                        <h3 class="text_attenddance-title3">
                            <span>08:00 AM</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>05:00 PM</span>
                        </h3>
                        <hr />
                        <div class="row">
                            <div class="col-6 text-center">
                                <button class="btn btn_attendance-clock md">Clock In</button>
                            </div>
                            <div class="col-6 text-center">
                                <button class="btn btn_attendance-clock md">Clock Out</button>
                            </div>
                        </div>
                        <div class="text-center" style="margin-top: 25px;">
                            <button class="btn btn-success btn_attendance-start_break md" style="">Slide to Start Break</button>
                        </div>
                        <div style="height:20px;">

                        </div>
                    </div>
                </div>
                <div class="content_attendance-riwayat">
                    <div class="row" style="margin-top: 15px;">
                        <div class="col-6">
                            <h4 class="" style="font-weight: bolder;">Riwayat</h4>
                        </div>
                        <div class="col-6">
                            <h6 class="text-right" style="margin-top: 10px; color: #4d93c7;">View All</h6>
                        </div>
                    </div>
                    <div>
                        <div id="riwayat" class="panel_attenddance-riwayat-hide panel_attenddance-riwayat">
                            <button class="panel_head_attenddance-riwayat" onclick="tooglePanelRiwayat(this)">Tue, 12 April 2022</button>
                            <p style="margin-top: 10px;"></p>
                            <div class="row">
                                <div class="col">
                                    <p class="ml-2 text_attenddance-datetime_now text-left">Persensi</p>
                                </div>
                            </div>
                            <div class="row mt-0 justify-content-between">
                                <div class="col-5">
                                    <p class="m-0 pl-5 text-list-announchement">08 : 00 AM</p>
                                </div>
                                <div class="col-3 text-center">
                                    <p class="m-0 p-0 text-list-announchement">Clock In</p>
                                </div>
                            </div>
                            <hr class="mt-0 mx-2">
                            <div class="row mt-0 justify-content-between">
                                <div class="col-5">
                                    <p class="m-0 pl-5 text-list-announchement">08 : 00 AM</p>
                                </div>
                                <div class="col-3 text-center">
                                    <p class="m-0 p-0 text-list-announchement">Start Break</p>
                                </div>
                            </div>
                            <hr class="mt-0 mx-2">
                            <div class="row mt-0 justify-content-between">
                                <div class="col-5">
                                    <p class="m-0 pl-5 text-list-announchement">08 : 00 AM</p>
                                </div>
                                <div class="col-3 text-center">
                                    <p class="m-0 p-0 text-list-announchement">Finish Break</p>
                                </div>
                            </div>
                            <hr class="mt-0 mx-2">
                            <div class="row mt-0 justify-content-between">
                                <div class="col-5">
                                    <p class="m-0 pl-5 text-list-announchement">08 : 00 AM</p>
                                </div>
                                <div class="col-3 text-center">
                                    <p class="m-0 p-0 text-list-announchement">Clock Out</p>
                                </div>
                            </div>
                            <hr class="mt-0 mx-2">
                            <!-- End Persensi -->

                            <!-- Location -->
                            <div class="row">
                                <div class="col">
                                    <p class="ml-2 text_attenddance-datetime_now text-left ">Location</p>
                                </div>
                            </div>
                            <div class="row mt-0 justify-content-between">
                                <div class="col-8">
                                    <p class="ml-2 text-list-announchement">
                                        jl.Yos Sudarso Kav. 33, Sunter Jaya, RT.10/RW.11, Sunter Jaya, Kec. Tj. Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta
                                    </p>
                                </div>
                                <div class="col-3 text-center">
                                    <p class="m-0 p-0 text-list-announchement">Clock In</p>
                                </div>
                            </div>
                            <hr class="mt-0 mx-2">
                            <div class="row mt-0 justify-content-between">
                                <div class="col-8">
                                    <p class="ml-2 text-list-announchement">
                                        jl.Yos Sudarso Kav. 33, Sunter Jaya, RT.10/RW.11, Sunter Jaya, Kec. Tj. Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta
                                    </p>
                                </div>
                                <div class="col-3 text-center">
                                    <p class="m-0 p-0 text-list-announchement">Clock Out</p>
                                </div>
                            </div>
                            <!-- End Location -->

                            <!-- Tugas -->
                            <div class="row mt-4">
                                <div class="col-2 mb-0 pb-0">
                                    <p class="ml-2 text_attenddance-datetime_now text-left mb-0 pb-0">Tugas</p>
                                </div>
                                <div class="col-10 text-right mt-0 pt-0">
                                    <button class="btn btn-primary rounded-pill mt-0 pt-0" type="button" style="width: 6rem;">Add</button>
                                </div>
                            </div>
                            <div class="row mt-0">
                                <div class="col text-center">
                                    <p class="m-0 pt-3 text-center">No Work</p>
                                </div>
                            </div>
                            <hr class="mt-0 mx-2">
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>

    <!-- jQuery  -->
    <script>
        function tooglePanelRiwayat(x) {
            document.getElementById('riwayat').classList.toggle('panel_attenddance-riwayat-show');
        }
    </script>