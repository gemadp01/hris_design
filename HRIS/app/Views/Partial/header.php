<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home</title>

    <link type="text/css" rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/icons.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/metisMenu.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/style.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/site.css" />
    <link type="text/css" rel="stylesheet" href="../assets/plugins/sweet-alert2/sweetalert2.min.css" />

    <!-- HOME -->
    <link type="text/css" rel="stylesheet" href="../assets/css/areas/home.css" />
    <!-- ATTENDANCE -->
    <link type="text/css" rel="stylesheet" href="../assets/css/areas/attenddance.css" />
    <!-- NOTIFICATION -->
    <link type="text/css" rel="stylesheet" href="../assets/css/areas/notification.css"/>
    <!-- Request -->
    <link rel="stylesheet" type="text/css" href="../assets/css/areas/request.css">
    
</head>
<body>
    <!--=============== HEADER ===============-->
    <header class="header" id="header">
        <div class="container">
            <nav class="nav ">
                <a href="../index" class="nav__logo">HRIS</a>

                <div class="nav__menu" id="nav-menu">
                    <ul class="nav__list">
                        <li class="nav__item">
                            <a href="../Personal/Index" class="nav__link">
                                <i class="nav__icon fas fa-user"></i>
                                <span class="nav__name">Personal</span>
                            </a>
                        </li>
                        
                        <li class="nav__item">
                            <a href="../Attendance/Index" class="nav__link">
                                <i class="nav__icon fas fa-calendar-alt"></i>
                                <span class="nav__name">Attendance</span>
                            </a>
                        </li>

                        <li class="nav__item">
                            <a href="../Request/Index" class="nav__link">
                                <i class="nav__icon fas fa-clipboard-list"></i>
                                <span class="nav__name">Request</span>
                            </a>
                        </li>

                        <li class="nav__item">
                            <a href="../Notification/Index" class="nav__link">
                                <i class="nav__icon fas fa-envelope"></i>
                                <span class="nav__name">Notification</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>