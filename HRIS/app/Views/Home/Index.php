﻿    <div class="">
        <main role="main">
            <div class="" style=" background: rgb(202,236,249); background: linear-gradient(180deg, rgba(202,236,249,1) 85%, rgba(255,255,255,1) 100%); ">
                <div class="container">
                    <div class="pt-3">
                        <p class="text_home-title1">Argo Manunggal Group</p>
                        <p class="text_home-title2">Selamat Pagi....</p>
                    </div>
            
                    <div class="mt-3">
                        <div class="row">
                            <div class="col-6">
                                <p class="text_home-fullname">Kamal Fasih</p>
                                <p class="text_home-role">Senior Accountant</p>
                            </div>
                            <div class="col-6 text-right mt-1">
                                <p class="text_home-title3 mb-0 mt-2">Payroll Current</p>
                                <p class="text_home-title3 m-0">Rp.XXXXXXXX</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel_home mt-3">
                    <div class="panel_home-icon">
                    </div>
                </div>
                <div class="" style="height: 50px;">
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <p class="text_home-title4">Annouchment</p>
                    </div>
                    <div class="col-6">
                        <p class="text_home-title5" style="text-align: right;">View All</p>
                    </div>
                </div>
                <div class="list_home-announcement">
                    <div class="row mt-0">
                        <div class="col-8">
                            <p class="text_home-list_announchement">Argo Manunggal Group - New Family Member - 12 April-2022</p>
                        </div>
                        <div class="col-4">
                            <p class="text-right" style="font-size: 10px;">12 April 2022</p>
                        </div>
                    </div>
                    <hr class="mt-0">
                    <div class="row mt-0">
                        <div class="col-8">
                            <p class="text_home-list_announchement">Argo Manunggal Group - New Family Member - 12 April-2022</p>
                        </div>
                        <div class="col-4">
                            <p class="text-right" style="font-size: 10px;">12 April 2022</p>
                        </div>
                    </div>
                    <hr class="mt-0">
                    <div class="row mt-0">
                        <div class="col-8">
                            <p class="text_home-list_announchement">Argo Manunggal Group - New Family Member - 12 April-2022</p>
                        </div>
                        <div class="col-4">
                            <p class="text-right" style="font-size: 10px;">12 April 2022</p>
                        </div>
                    </div>
                    <hr class="mt-0">
                    <div class="row mt-0">
                        <div class="col-8">
                            <p class="text_home-list_announchement">New Office Rules</p>
                        </div>
                        <div class="col-4">
                            <p class="text-right" style="font-size: 10px;">12 April 2022</p>
                        </div>
                    </div>
                    <hr class="mt-0">
                </div>
            </div>
        </main>
    </div>

