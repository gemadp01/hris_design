﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PayRoll History</title>

    <link type="text/css" rel="stylesheet" href="../assets/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/icons.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/metisMenu.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/style.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/site.css" />
    <link type="text/css" rel="stylesheet" href="../assets/plugins/sweet-alert2/sweetalert2.min.css" />
</head>
<body>
    <!--=============== HEADER ===============-->
    <header class="header" id="header">
        <div class="container">
            <nav class="nav ">
                <a href="/Index" class="nav__logo">HRIS</a>

                <div class="nav__menu" id="nav-menu">
                    <ul class="nav__list">
                        <li class="nav__item">
                            <a href="../Personal/Index.html" class="nav__link">
                                <i class="nav__icon fas fa-user"></i>
                                <span class="nav__name">Personal</span>
                            </a>
                        </li>
                        
                        <li class="nav__item">
                            <a href="../Attendance/Index.html" class="nav__link">
                                <i class="nav__icon fas fa-calendar-alt"></i>
                                <span class="nav__name">Attendance</span>
                            </a>
                        </li>

                        <li class="nav__item">
                            <a href="../Request/Index.html" class="nav__link">
                                <i class="nav__icon fas fa-clipboard-list"></i>
                                <span class="nav__name">Request</span>
                            </a>
                        </li>

                        <li class="nav__item">
                            <a href="../Approval/Index.html" class="nav__link">
                                <i class="nav__icon fas fa-clipboard-check"></i>
                                <span class="nav__name">Approval</span>
                            </a>
                        </li>

                        <li class="nav__item">
                            <a href="../Notification/Index.html" class="nav__link">
                                <i class="nav__icon fas fa-envelope"></i>
                                <span class="nav__name">Notification</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>

    <div class="container">
        <main role="main">
            
        </main>
    </div>

    <!-- jQuery  -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>

    <script src="../assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
    <script src="../assets/js/site.js" asp-append-version="true"></script>
</body>
</html>
