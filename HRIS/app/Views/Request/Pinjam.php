<div class="container">
	<div class="kotak">
	<label>Request Pinjaman</label>
	<hr>
		<form action="#" method="post">
			<div class="row">
				<div class="col-3"><label>Kode Pinjaman</label></div>
				<div class="col-9"><input type="text" name="kode" class="input-text" placeholder="Kode Pinjaman" required="required"></div>
			</div>
			<div class="row">
				<div class="col-3"><label>Id Pegawai</label></div>
				<div class="col-9"><input type="text" name="id_pegawai" class="input-text" placeholder="Id Pegawai" required="required"></div>
			</div>
			<div class="row">
				<div class="col-3"><label>Tanggal Permohonan</label></div>
				<div class="col-9"><input type="date" name="tanggal_permohonan" class="input-text" placeholder="2022-02-20" required="required"></div>
			</div>
			<div class="row">
				<div class="col-3"><label>Jumlah Pinjaman</label></div>
				<div class="col-9"><input type="text" name="lama_cuti" class="input-text" placeholder="Jumlah Pinjaman" required="required"></div>
			</div>
			<div class="row">
				<div class="col-3"><label>Limit Pinjaman</label></div>
				<div class="col-9"><input type="text" name="dari_tanggal" class="input-text" placeholder="Limit Pinjaman" required="required"></div>
			</div>
			<div class="row">
				<div class="col-3"><label>Lama Angsuran</label></div>
				<div class="col-9"><input type="text" name="sampai_tanggal" class="input-text" placeholder="Lama Angsuran" required="required"></div>
			</div>
			<div class="row">
				<div class="col-3"><label>Keterangan</label></div>
				<div class="col-9"><input type="text" name="keterangan" class="input-text" placeholder="Keterangan" required="required"></div>
			</div>
			<div class="row">
				<div class="col-3"></div>
				<div class="col-9"><input type="submit" class="submit_request" value="Submit Request"></div>
			</div>
		</form>
		
	</div>
	</div>