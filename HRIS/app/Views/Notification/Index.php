﻿<div class="container">
        <main role="main">
            <div class="">
                <div class="container">
                    <div class="tab-notifications pt-3">
                        <div class="row">
                            <div class="col-5 text-center">
                                <button id="tab-notifications-btn-notification" class="tab-notifications-btn tab-notification-btn-active" onclick="setTab('notification')"><p>Notification</p></button>
                            </div>
                            <div class="col-7 text-center">
                                <button id="tab-notifications-btn-approval" class="tab-notifications-btn tab-notification-btn-nonactive" onclick="setTab('Approval')"><p>Need My Approval</p></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container mt-4">
                    <div class="tab-notifications-content-notification">
                        <div class="item-notifications-content-notification">
                            <div class="row panel-notifications-content-notification">
                                <div class="col-3">
                                    <img class="img_notification" src="../assets/images/users/user-1.jpg" alt="Alternate Text"/>
                                </div>
                                <div class="col-9" style="padding: 0;">
                                    <p class="text-right text_notication-title1">12 April, 2022</p>
                                    <p class="text_notification-title2">Employee - Konduite - Request Approval</p>
                                    <p class="text_notification">Employee Konduite Request KDTREQ 2134568 Published, Please check "Temas ESS Apps"</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-notifications-content-approval tab-notification-hide">
                        <div class="item-notifications-content-approval">
                            <div id="request" class="panel-notifications-content-approval" >
                                <div class="row" style="padding-top: 7px;">
                                    <div class="col">
                                        <p class="text-right text_notication-title1">12 April, 2022</p>
                                    </div>
                                    
                                </div>
                                <div class="row" onclick="tooglePanelApproval(this)">
                                    <div class="col-3" style="padding-left: 20px;">
                                        <img class="img_notification" src="../assets/images/users/user-1.jpg" alt="Alternate Text"/>
                                    </div>
                                    <div class="col-9" style="padding: 0;">
                                        <p class="text_notification-title2">Booking Leave - Request</p>
                                        <table class="text_notification">
                                            <tr>
                                                <td><p class="text_notification">Request Number </p></td>
                                                <td><p class="text_notification">: OTREG 2111.0001</p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text_notification">Request by </p></td>
                                                <td><p class="text_notification">: IHSQ - 010321 - 0006 - Kojari</p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text_notification">Request Date </p></td>
                                                <td><p class="text_notification">: 12 - april 2022</p></td>
                                            </tr>
                                            <tr>
                                                <td><p class="text_notification">Status </p></td>
                                                <td><p class="text_notification">: CHECKED</p></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                
                                <div class="row" style="padding-left: 20px; padding-right: 20px;">
                                    <div class="col">
                                        <hr style="margin: 15px,10px;">
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div style="margin: 0; padding-left: 20px; ">
                                            <p class="text_notification">Request Information</p>
                                            <p class="text_notification">Leave type : Compassionate Leave of Spouse / Child/ Parents / In - Laws</p>
                                            <br style="margin: 0;">
                                            <p class="text_notification">Leave Duration   : OTREG 2111.0001</p>
                                            <p class="text_notification">Leave Start Date : IHSQ - 010321 - 0006 - Kojari</p>
                                            <p class="text_notification">Leave End Date   : 12 - april 2022</p>
                                            <p class="text_notification">Leave Credit Use : 2</p>
                                            <p class="text_notification">Description : Ayah Meninggal</p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row" style="padding: 5px 0px;">
                                    <div class="col-6 text-center">
                                        <button class="btn btn-notification-approve">Approve</button>
                                    </div>
                                    <div class="col-6 text-center">
                                        <button class="btn btn-notification-reject">Reject</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>

    <!-- jQuery  -->
    <script>
        function setTab(tabName) {
            if (tabName == "notification") {
                _setTab('#tab-notifications-btn-notification', '#tab-notifications-btn-approval', '.tab-notifications-content-notification', '.tab-notifications-content-approval');
            } else {
                _setTab('#tab-notifications-btn-approval', '#tab-notifications-btn-notification', '.tab-notifications-content-approval', '.tab-notifications-content-notification');
            }
        }
    
        function _setTab(tabShow, tabHidden, tabContentShow, tabContenHidden) {
            $(tabContentShow).removeClass("tab-notification-hide");
            $(tabContenHidden).addClass("tab-notification-hide")
            $(tabShow).removeClass("tab-notification-btn-nonactive");
            $(tabShow).addClass("tab-notification-btn-active");
            $(tabHidden).addClass("tab-notification-btn-nonactive");
            $(tabHidden).removeClass("tab-notification-btn-active");
        }
    </script>

    <script>
        function tooglePanelApproval(x) {
            document.getElementById('request').classList.toggle('panel-notifications-content-approval-show');
        }
    </script>