<?php 
    namespace App\Models;

    use CodeIgniter\Model;

    class RefResignModel extends Model
    {
        protected $table      = 'ref_permohonan_resign';
        protected $primaryKey = 'ID_PERMOHONAN_RESIGN';

        protected $useAutoIncrement = true;

        protected $returnType     = 'array';
        // protected $useSoftDeletes = true;

        //protected $allowedFields = ['name', 'email'];

        //protected $useTimestamps = false;
        //protected $createdField  = 'created_at';
        //protected $updatedField  = 'updated_at';
        //protected $deletedField  = 'deleted_at';

        //protected $validationRules    = [];
        //protected $validationMessages = [];
        //protected $skipValidation     = false;

        public function getData($id = false){
            if(!$id){
                return $this->findAll();
            }

            return $this->where(['ID_PERMOHONAN_RESIGN' => $id])->first();
        }

        // public function SaveData($param, $tipe = 1){
            
        // }
    }
?>