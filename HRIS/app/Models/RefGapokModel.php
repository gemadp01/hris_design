<?php 
    namespace App\Models;

    use CodeIgniter\Model;

    class RefGapokModel extends Model
    {
        protected $table      = 'ref_gapok';
        protected $primaryKey = 'ID_GAPOK';
        protected $useAutoIncrement = true;
        protected $returnType     = 'array';
        // protected $useSoftDeletes = true;

        //protected $allowedFields = ['name', 'email'];

        //protected $useTimestamps = false;
        //protected $createdField  = 'created_at';
        //protected $updatedField  = 'updated_at';
        //protected $deletedField  = 'deleted_at';

        //protected $validationRules    = [];
        //protected $validationMessages = [];
        //protected $skipValidation     = false;

        public function getData($id = false){
            if(!$id){
                return $this->findAll();
            }

            return $this->where(['ID_GAPOK' => $id])->first();
        }
    }
?>